#MAC0110 - MiniEP10
#Gustavo Korzune Gurgel - 4778350

function soma_array(array)
    n_rows = size(array)[1]
    n_cols = size(array)[2]
    sum = 0
    for i = 1:n_rows
        for j = 1:n_cols
            sum += array[i, j]
        end
    end
    return sum
end

using Test

function testsoma_array()
    @test soma_array([5 5 5]) == 15
    @test soma_array([1 2 3; 4 5 6]) == 21
    @test soma_array([23 -3 1000; -69 -13 420]) == 1358
    print("fim dos testes")
end

testsoma_array()

function create_sub_matrix(matrix, sub_size, inic_row, inic_col)
    sub_matrix = zeros(Int, sub_size, sub_size)
    element = 1
    for j = inic_col:(inic_col + sub_size -1)
        for i = inic_row:(inic_row + sub_size - 1)
            sub_matrix[element] = matrix[i, j]
            element += 1
        end
    end
    return sub_matrix
end

function test_create_s_m()
    A = [1 2 3; 4 5 6; 7 8 9]
    @test create_sub_matrix(A, 1, 1, 3) == fill(3, (1, 1))
    @test create_sub_matrix(A, 2, 1, 1) == [1 2; 4 5]
    @test create_sub_matrix(A, 2, 2, 1) == [4 5; 7 8]
    @test create_sub_matrix(A, 3, 1, 1) == A
    print("fim dos testes")
end

test_create_s_m()

function max_sum_submatrix(matrix)
    size_m = size(matrix)[1]
    answers = [[0 0 0 0]]
    #answers = [[sub_size inic_row inic_col sum]]
    maior = -Inf
    for sub_size = 1:size_m
        for inic_row = 1:(size_m - sub_size + 1)
            for inic_col = 1:(size_m - sub_size + 1)
                sub_matrix = create_sub_matrix(matrix, 
                sub_size, inic_row, inic_col)
                sum = soma_array(sub_matrix)
                if !(sum < maior)
                    append!(answers, [[sub_size inic_row inic_col sum]])
                    maior = sum
                end
            end
        end
    end

    deleteat!(answers, 1)
    i = 1
    while i <= size(answers)[1]
        if answers[i][4] == maior
            sub_size = answers[i][1]
            inic_row = answers[i][2]
            inic_col = answers[i][3]
            answers[i] = create_sub_matrix(matrix, 
            Int(sub_size), Int(inic_row), Int(inic_col))
            i += 1
        else
            deleteat!(answers, i)
        end
    end
    return answers
end

function testmax_sum_sub_matrix()
    @test max_sum_submatrix([1 1 1 ; 1 1 1 ; 1 1 1]) == [[1 1 1 ; 1 1 1 ; 1 1 1]]
    @test max_sum_submatrix([0 -1 ; -2 -3]) == [fill(0,(1,1))]
    @test max_sum_submatrix([-1 1 -1 ; 1 -1 1 ; 1 1 -2]) == [[1 -1; 1 1]]
    @test max_sum_submatrix([-1 1 1 ; 1 -1 1 ; 1 1 -2]) == [[1 1; -1 1], [1 -1; 1 1], [-1 1 1; 1 -1 1; 1 1 -2]]
    print("fim dos testes")
end

testmax_sum_sub_matrix()